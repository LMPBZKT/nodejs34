const clientes = [
    {
        id: 1,
        nombre: 'Leonor'
    },
    {
        id: 2,
        nombre: 'Jasinto'
    },
    {
        id: 3,
        nombre: 'Waldo'
    },
]

const pagos = [
    {
        id: 1,
        pago: 1000,
        moneda: 'Bs'
    },
    {
        id: 2,
        pago: 1800,
        moneda: 'Bs'
    }
]

const id = 1;
const getClientes = (id) => {
    return new Promise((resolve, reject) => {
        const cliente = clientes.find(e => e.id === id);
        if (cliente) {
            resolve(cliente);
        } else {
            reject(`No se encuentrar las especificaciones de la id ${id}`);
        }
    });
};


const getPagos = (id) => {
    return new Promise((resolve, reject) => {
        const pago = pagos.find(p => p.id === id);
        if (pago) {
            resolve(pago);
        } else {
            reject(`No existe pago con el id ${id}`);
        }
    });
};


getClientes(id)
    .then(cliente => {
        getPagos(id)
            .then(pago => {
                console.log('El cliente de id:', cliente.nombre, 'tiene un pago de:', pago.pago, pago.moneda);
            })
            .catch(error => {
                console.log(error);
            });;
    })

getClientes(id)
    .then(cliente => console.log(cliente))
    .catch(error => {
        console.log(error);
    });

getPagos(id)
    .then(pago => console.log(pago))
    .catch(error => {
        console.log(error);
    });

let nombre;


getClientes(id)
    .then(cliente => {
        nombre = cliente.nombre;
        return getPagos(id);
    })
    .then(pago => {
        console.log('El empleado:',nombre,'\nid:',id,'\nPago:',pago['pago'],'\nMoneda:',pago['moneda']);

    })

    .catch(error => {
        console.log(error);
    })


