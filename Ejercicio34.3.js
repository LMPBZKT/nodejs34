const clientes = [
    {
        id: 1,
        nombre: 'Leonor'
    },
    {
        id: 2,
        nombre: 'Jasinto'
    },
    {
        id: 3,
        nombre: 'Waldo'
    },
]

const pagos = [
    {
        id: 1,
        pago: 1000,
        moneda: 'Bs'
    },
    {
        id: 2,
        pago: 1800,
        moneda: 'Bs'
    }
]

const getClientes = (id, callback) => {
    const cliente = clientes.find(e => e.id === id);
    if (cliente) {
        callback(null, cliente);
    } else {
        callback(`Cliente con el id ${id} no existe`);
    }
};

getClientes(2, (err, cliente) => {
    if (err) {
        console.log('ERROR!');
        return console.log(err);
    }
    console.log('Cliente existe');
    console.log(cliente);
});

const getPagos = (id, callback) => {
    const pago = pagos.find(e => e.id === id);
    if (pago) {
        callback(null, pago);
    } else {
        callback(`El pago de  ${id} no existe`);
    }
};

getPagos(2, (err, pago) => {
    if (err) {
        console.log('ERROR!');
        return console.log(err);
    }
    console.log('Pago existe');
    console.log(pago);
});