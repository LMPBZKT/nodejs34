const clientes = [
    {
        id: 1,
        nombre: 'Leonor'
    },
    {
        id: 2,
        nombre: 'Jasinto'
    },
    {
        id: 3,
        nombre: 'Waldo'
    },
]

const pagos = [
    {
        id: 1,
        pago: 1000,
        moneda: 'Bs'
    },
    {
        id: 2,
        pago: 1800,
        moneda: 'Bs'
    }
]

const id = 4;

const getClientes = (id, callback) => {
    const cliente = clientes.find(e => e.id === id);
    if (cliente) {
        callback(null, cliente);
    } else {
        callback(`Cliente con id ${id} no existe`);
    }
};
getClientes(1, (err, cliente) => {
    if (err) {
        console.log('ERROR!');
        return console.log(err);
    }
    console.log('Cliente existe');
    console.log(cliente);
});


const getPagos = (id, callback) => {
    const pago = pagos.find(s => s.id === id);
    if (pago) {
        callback(null, pago);
    } else {
        callback(`No existe un pago para el id ${id}`);
    }
};

getPagos(id, (err, pago) => {
    if (err) {
        console.log('ERROR!');
        return console.log(err);
    }
    console.log(pago);
});


getClientes(id, (err, cliente) => {
    if (err) {
        console.log('ERROR!');
        return console.log(err);
    }
    console.log('Cliente existe');
    console.log(cliente);
    getPagos(id, (err, pago) => {
        if (err) {
            console.log('ERROR!');
            return console.log(err);
        }
        console.log('El cliente:', cliente, 'tiene un pago de:', pago);
    });
});
