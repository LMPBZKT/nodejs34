const clientes = [
    {
        id: 1,
        nombre: 'Leonor'
    },
    {
        id: 2,
        nombre: 'Jasinto'
    },
    {
        id: 3,
        nombre: 'Waldo'
    },
]

const pagos = [
    {
        id: 1,
        pago: 1000,
        moneda: 'Bs'
    },
    {
        id: 2,
        pago: 1800,
        moneda: 'Bs'
    }
]

const id = 2;
const getClientes = (id) => {
    return new Promise((resolve, reject) => {
        const cliente = clientes.find(e => e.id === id);
        if (cliente) {
            resolve(cliente);
        } else {
            reject(`No se encuentrar las especificaciones de la id ${id}`);
        }
    });
};


const getPagos = (id) => {
    return new Promise((resolve, reject) => {
        const pago = pagos.find(p => p.id === id);
        if (pago) {
            resolve(pago);
        } else {
            reject(`No existe pago con el id ${id}`);
        }
    });
};


const getClientes1 = async (id) => {
    try{
        const cliente = await getClientes (id);
        const pago = await getPagos (id);
        return `id: ${cliente,id}\ncliente:${cliente.nombre} \npago:${pago.pago} ${pago.moneda}`
    }
    catch(e){
        throw e;
    }
};

getClientes1(id)
.then (mensage => console.log(mensage))
.catch(error => console.log(error))